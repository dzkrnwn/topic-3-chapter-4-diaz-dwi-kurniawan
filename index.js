const fs = require("fs");
const data = require("./data.json");
const bola = require("./src/bola");
const kubus = require("./src/kubus");
const limas = require("./src/limas");
const {keliling} = require("./src/kubus")

kubus.luas(10);
kubus.volume(5);

bola.luas(9);
bola.volume(5);

limas.luas(4, 5);
limas.volume(3, 9);